branchFileName="branches.txt"

git branch -r | sed 's/origin\///g; s/^ *//g' | grep -v HEAD > "$branchFileName"

while IFS= read -r branchName
do
        echo "Getting updates from branch: $branchName"
	git checkout "$branchName"
	git pull
done <"$branchFileName"

rm "$branchFileName"
